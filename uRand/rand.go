package uRand

import (
	"math/rand"
	"time"
)

func NewRandInt(n int) int {
	rand.Seed(time.Now().Unix())

	return rand.Intn(n)
}
