# Tools
工具库

## 安装
```shell script
go get gitee.com/gomods/tools
```

## Address
```go
import "gitee.com/gomods/tools/uAddress"

// 获取IP
ips, err := uAddress.IntranetIP()

```

## Os
```go
import "gitee.com/gomods/tools/uOs"

// 判断文件是否存在
has, err := uOs.DirExists("/home/dev")
```

## Rand
```go
import "gitee.com/gomods/tools/uRand"

// 生产 [100] 以内随机数
r := uRand.NewRandInt(1000)
```

## Snowflake 

### 初始化
```go
import "gitee.com/gomods/tools/uSnowflake"

// 初始化，传递项目名称
uSnowflake.InitSnowflake("appName")
```

### 示例
```go
import "gitee.com/gomods/tools/uSnowflake"

// 生产雪花ID
uSnowflake.NewIdInt64()
uSnowflake.NewIdString()
```