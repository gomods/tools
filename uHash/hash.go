package uHash

// 字符串哈希算法
func BucketHash(s string, m int) int {
	seed := 131
	strlen := len(s)
	hash := 0
	for i := 0; i < strlen; i++ {
		hash = hash*seed + int(s[i])
	}
	hash = hash & 0x7FFFFFFF
	return hash % m
}
