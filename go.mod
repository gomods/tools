module gitee.com/gomods/tools

go 1.15

require (
	gitee.com/gomods/redisdao v0.0.1
	github.com/bwmarrin/snowflake v0.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/onsi/ginkgo v1.16.3 // indirect
	github.com/onsi/gomega v1.13.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
