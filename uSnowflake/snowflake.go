package uSnowflake

import (
	"context"
	"fmt"

	"gitee.com/gomods/tools/uSnowflake/redisRepo"

	"github.com/bwmarrin/snowflake"
)

var (
	snowflakeNode *snowflake.Node
)

func InitSnowflake(typ string) {

	node := redisRepo.NewSnowflakeRepo().MakeSnowflakeNode(context.Background(), typ) % 1024
	fmt.Println("snowflakeNode:", node)

	var err error
	snowflakeNode, err = snowflake.NewNode(node)

	if err != nil {
		panic(err)
	}
}

func NewIdInt64() int64 {
	return snowflakeNode.Generate().Int64()
}

func NewIdString() string {
	return snowflakeNode.Generate().String()
}
