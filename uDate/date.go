package uDate

import (
	"time"
)

const TIME_FORMAT = "2006-01-02 15:04:05"

// UnixMillis 取当前系统时间的毫秒
func UnixMillis() int64 {
	t := time.Now()
	return t.UnixNano() / 1000000
}

// CurrentDate 获取当前日期 - 20210130
func CurrentDate() string {
	return time.Now().Format("20060102")
}

// BeforeDayDate 获取前一天日期
func BeforeDayDate() string {
	return time.Now().AddDate(0, 0, -1).Format("20060102")
}

// AfterDayDateUnix 获取第二天凌晨时间戳
func AfterDayDateUnix() int64 {
	aTime, err := time.ParseInLocation("2006-01-02 15:04:05", time.Now().Format("2006-01-02")+" 23:59:59", time.Local)
	if err != nil {
		// 如果出错，直接偏移到第二天
		return time.Now().AddDate(0, 0, 1).Unix()
	}

	return aTime.Unix() + 1
}

// AfterDayToNowUnixDiff 获取距离凌晨的时间戳
func AfterDayToNowUnixDiff() int64 {
	return AfterDayDateUnix() - time.Now().Unix()
}
